const productsSection = document.querySelector('main section')
const product_id = getIdFromQuery()

getProduct(product_id)
    .then(showProduct)

function getProduct(id){
    return fetch(`${API_URL}/${id}`)
            .then(res => res.json())
}

function showProduct(product){
    const buttons = `<a href="./edit.html?id=${product.id}" class="btn btn-success">Edit Product</a>
                     <button id="deleteButton" class="btn btn-danger">Delete Product</a>`
    addProductToPage(product,12,buttons, productsSection)

    const deleteButton = document.querySelector('#deleteButton')
    deleteButton.addEventListener('click',()=>{
        deleteProduct(product_id)
            .then(()=>{
                window.location = '/client'
            })
    })
}

function deleteProduct(id){
    return fetch(`${API_URL}/${id}`,{
        method: 'DELETE',               
    }).then(res =>res.json())
}


