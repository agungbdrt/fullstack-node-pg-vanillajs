
module.exports = [{
    title: 'Macbook Pro',
    description: 'A laptop for cool people.',
    price: 999.99,
    quantity: 10,
    image: 'asset/images/mcbookpro.jpg'
  }, {
    title: 'Skateboard',
    description: 'Full setup! Get skateboarding today!',
    price: 99.99,
    quantity: 5,
    image: 'asset/images/skateboard.jpg'
  }, {
    title: 'Speaker System',
    description: 'Listen to music and things real loud!',
    price: 199.99,
    quantity: 2,
    image: 'asset/images/speaker.png'
  }, {
    title: 'T-Shirt',
    description: 'Plane, white, T.',
    price: 9.99,
    quantity: 20,
    image: 'asset/images/tshirt.jpg'
  }, {
    title: 'Shoes',
    description: 'Vans shoes for skateboarding, or not.',
    price: 49.99,
    quantity: 5,
    image: 'asset/images/shoes.jpg'
  }];